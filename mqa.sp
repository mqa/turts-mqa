/*
TODO
-MQA messages with their own prefix
-Add enabling disabling

-subbing
-parsing
-elimination
-tdm

-shuffle with sum to queue width logic, followed by less than logic
*/

/*
code cleanup TODO
create a multiQueue function to check separate spawns and even queues
remove camelCase
change check* functions to is* functions
*/

/*
Queues are layed out as such in the menu

ex. 3v3
empty, empty, empty    #Will be dequeued third
empty, thenoid, empty  #Will be dequeued second
turts, DeuceBox, empty #Will be dequeued first

there should never be an empty queue position in between queue positions with players
there should always be one empty queue position available 

ex. Deuce switches to queue position 3
empty, empty, empty     #Queue position 4 becomes available
DeuceBox, empty, empty    
empty, thenoid, empty  
turts, empty, empty 

When a player adds/removes, there is 15s period given before a shuffle takes place
ex. Shuffle result for above queue
empty, empty, empty
turts, thenoid, DeuceBox
*/

#include <sourcemod>
#include <tf2>
#include <sdkhooks>
#include <tf2_stocks>
#include <sdktools>

#define PLUGIN_NAME		"MQA"
#define PLUGIN_VERSION 	"0.2.0"

#define MAX_PLAYERS     50
#define MAX_ARENAS 		25

//TODO probably remove this
#define TRUE            1
#define FALSE           0

//Used in checkActiveArenaReady and checkInactiveArenaReady
#define NOT_READY       0
//For inactiveArenas
#define READY           1
//For activeArenas - used 2 4 so they can be ANDed with functions - see function TODO
#define REPLACE_WINNER  2
#define REPLACE_LOSER   4

//TODO This is a guess
#define RED             0
#define BLU             1

//Clientid in a queue position symbolizing empty slot
#define EMPTY_SLOT      0

//Gametypes
#define DEATHMATCH      0
#define ELIMINATION     1
#define BBALL           2

//Used when looking for a player
#define PLAYER_IDLE     0
#define PLAYER_ACTIVE   1
#define PLAYER_QUEUED   2

//Arena States
#define INACTIVE        0
#define ACTIVE          1

//Player States
#define DEAD            0
#define ALIVE           1


public Plugin:myinfo = 
{
	name = PLUGIN_NAME,
	author = "DeuceBox",
	description = "Multi-Queue Arenas",
	version = PLUGIN_VERSION,
	url = "http://steamcommunity.com/id/DeuceBox"
}

new Handle:cvar_enabled = INVALID_HANDLE,
	Handle:cvar_debug = INVALID_HANDLE;

//options	
new bool:options_enabled,
	bool:options_debug;
	
enum Opt
{
    //Players don't pick a team unless queues are uneven or teams have separate spawns
	even_queues,
	separate_spawns,
    point_limit,
    game_type,
    //red health, blu health, spawn rate, infinite ammo,
};

//TODO there should be a better way to do this, but have to define number of player details to avoid tag mismatch when
//passing to findPlayer
#define PLAYERDETAILS   3
enum PlayerDetails
{
    //Weird names because the normal ones are already used a lot
    arena_ix,
    team_ix,
    queue_position_ix,
};

enum Coord
{
    x,
    y,
};

//someone added to queue
//are the qpos1s full
//set active
//not full
//spawn person who entered the queue

//set active
//toggle active bool
//set all qpos1 to spectate
//dequeue
//respawn actives

//respawn hook
//is active
//
//is not active
//is person in qpos1 
//allow

//Arena name
new String:arena[MAX_ARENAS][192];
//Separate spawns, TODO
new options[MAX_ARENAS][Opt];
//Has a match been started in that arena TODO could be a state
new arena_state[MAX_ARENAS];
//Each index stores a queue that has a length and width
//The width is fixed, the length depends on the number of people in the queue
new Handle:queue[2][MAX_ARENAS];
//Each arena index will hold 2, dequeued, queue positions which are the active players
new Handle:active_players[2][MAX_ARENAS];
//Each arena gets an player state array to keep track if player is alive or dead
new player_state[MAX_PLAYERS];
//Points awarded for kills or team eliminations or captured flags - point limit is an arena option
new points[2][MAX_ARENAS];
//Each arena gets a subtitution array, where people can sub in if the active team loses a player
new Handle:subs[2][MAX_ARENAS];
//How many players can each queue slot hold
new qwidth[2][MAX_ARENAS];

//Spawn points for each team
new Handle:spawn_points[2][MAX_ARENAS];
//Spawn orientation for each team
new Handle:spawn_orientation[2][MAX_ARENAS]
//Will be used to cycle through a team's spawn points so players don't spawn on top of each other
new spawn_index[2][MAX_ARENAS]

//spawn points should be created as such
//new point[Coord]
//new orientation[Angles]
//read in spawn point xy and orientation for given team and arena_index
//point[x] = x read from file
//point[y] = y read from file
//orientation[pitch] = pitch read from file
//...
//pushArrayArray([spawn_point[team][arena_index], point)
//pushArrayArray([spawn_orienation[team][arena_index], orientation)



	
new arena_count;

public OnPluginStart()
{
	//						    command	   default val   description
	cvar_enabled = CreateConVar("mqa_enabled", "1", "Enables or disables the plugin (1/0)."); //TODO
	cvar_debug = CreateConVar("mqa_debug", "1", "Enables or disables the plugins debug mode (1/0).");
	
	RegConsoleCmd("version", cmdVersion, "Displays plugin version.");
	RegConsoleCmd("say", cmdSay);
	RegConsoleCmd("say_team", cmdSay);

	//Hook the convar so we know when the command gets changed
	HookConVarChange(cvar_enabled, cvarCMDChanged);	
	HookConVarChange(cvar_debug, cvarCMDChanged);
	
	//SIMULATED ARENA DATA - START

    //this will be parsed in from a file
    new queue_sizes1[5] = {1,2,3,4,5};
    new queue_sizes2[5] = {1,2,4,4,5};
    new queue_options[5][Opt];

    new String:arena_names[5][192];
	
	arena_names[0] = "arena1";
	arena_names[1] = "arena2";
	arena_names[2] = "arena3";
	arena_names[3] = "arena4";
	arena_names[4] = "arena5";
	
    //even_queues should be calculated not specified
	queue_options[0][even_queues] = 1;
	queue_options[0][separate_spawns] = 0;
	queue_options[1][even_queues] = 1;
	queue_options[1][separate_spawns] = 0;
	queue_options[2][even_queues] = 0;
	queue_options[2][separate_spawns] = 0;
	queue_options[3][even_queues] = 1;
	queue_options[3][separate_spawns] = 0;
	queue_options[4][even_queues] = 1;
	queue_options[4][separate_spawns] = 0;
	
	for (new i = 0; i < MAX_ARENAS; i++)
	{
        //put a row of fake clients in each arena
		arena_count = arena_count + 1;
		arena[i] = arena_names[i];
		
        queue[0][i] = CreateArray(queue_sizes1[i]);
        queue[1][i] = CreateArray(queue_sizes2[i]);
        
        new queue_clients1[queue_sizes1[i]];
        for (new j = 0; j < queue_sizes1[i]; j++)
        {
            //give simulated clients an arbitrary clientid
            queue_clients1[j] = j*50;
        }
        qwidth[0][i] = queue_sizes1[i];
        PushArrayArray(queue[0][i], queue_clients1);
        PushArrayArray(queue[0][i], queue_clients1);
        
        new queue_clients2[queue_sizes2[i]];
        for (new j = 0; j < queue_sizes2[i]; j++)
        {
            queue_clients2[j] = j*50;
        }
        qwidth[1][i] = queue_sizes2[i];
        PushArrayArray(queue[1][i], queue_clients2);
        PushArrayArray(queue[1][i], queue_clients2);
			
		options[i][separate_spawns] = queue_options[i][separate_spawns];
		options[i][even_queues] = queue_options[i][even_queues];
	}

    //put some more fake clients in
	new queue_clients3[4] = {30, 40, 0, 0};
	new queue_clients4[4] = {60, 0, 0, 0};
	new queue_clients5[4] = {0, 70, 80, 0};
	new queue_clients6[4] = {0, 0, 90, 0};
	PushArrayArray(queue[1][2], queue_clients3);
	PushArrayArray(queue[1][2], queue_clients4);
	PushArrayArray(queue[1][2], queue_clients5);
	PushArrayArray(queue[1][2], queue_clients6);
	
    //not actually necessary - just used to test the function
	queueCleanup();

	//SIMULATED ARENA DATA - START
}

public Action:cmdSay(client_id, args)
{
    decl String:text[192];
    GetCmdArgString(text, sizeof(text));

    new startidx = 0;
    if (text[0] == '"')
    {
        startidx = 1;

        new len = strlen(text);
        if (text[len-1] == '"')
        {
            text[len-1] = '\0';
        }
    }

    if (strcmp(text[startidx], "!debug") == 0)
    {		
		queueDebug(client_id);

        return Plugin_Handled;
    }
	else if (strcmp(text[startidx], "!add") == 0)
	{
		menuArenas(client_id)
		
		return Plugin_Handled;
	}
	else if (strcmp(text[startidx], "!shuffle") == 0)
	{
		//shuffle(arena_index, team) if need be;
		return Plugin_Handled;
	}
	else
	{
		debugMessage(text);
		return Plugin_Continue;
	}
}  

//Setup and Display Arena Selection Menu
public menuArenas(client_id)
{
	new Handle:menu = CreateMenu(arenaMenuHandler);
	SetMenuTitle(menu, "Select an Arena");
	for (new arena_index = 0; arena_index < arena_count; arena_index++)
	{
		decl String:info[32];
		IntToString(arena_index, info, sizeof(info));
		AddMenuItem(menu, info, arena[arena_index]);
	}
	SetMenuExitButton(menu, true);
	DisplayMenu(menu, client_id, 20);
}

public arenaHasSharedSpawns(arena_index)
{
    if ((options[arena_index][separate_spawns] == FALSE) && (options[arena_index][even_queues] == TRUE))
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

public arenaMenuHandler(Handle:menu, MenuAction:action, client_id, param)
{
	if (action == MenuAction_Select)
	{
		new String:info[32];
		GetMenuItem(menu, param, info, sizeof(info));
		new arena_index = StringToInt(info);
        
		if (!arenaHasSharedSpawns(arena_index))
		{
            //Bring up team menu because they have to decide if they want RED spawns or BLU spawns
            //or if they want to be on the team with more or less players
			menuTeams(client_id, arena_index);
		}
		else if (qwidth[0][arena_index] > 1) 
		{
            //Teams share the same spawns and are the same size so they just need to pick a queue position
            //Thus, there is a single queue feeding the arena, qwidth[1][arena_index] is unused 
            //RED team's queue will be used as the single queue
			menuQueue(client_id, arena_index, RED);
		}
		else
		{
            //1v1 so queue up players at the back of the line
			addToQueue(client_id, arena_index, 1, -1);
		}
	}
	else if (action == MenuAction_End)
	{
		CloseHandle(menu);
	}
}

//Setup and Display Team Selection Menu
public menuTeams(client_id, arena_index)
{
	new Handle:menu = CreateMenu(teamMenuHandler);
	
	decl String:title[192];
	Format(title, sizeof(title), "Select a team: RED %dvs%d BLU", qwidth[0][arena_index], qwidth[1][arena_index]);
	
    //Pass need to pass arena index and selected team info to next menu
	decl String:info1[192];
	Format(info1, sizeof(info1), "%d|%d", arena_index, RED);
	
	decl String:info2[192];
	Format(info2, sizeof(info2), "%d|%d", arena_index, BLU);
	
	SetMenuTitle(menu, title);
	AddMenuItem(menu, info1, "RED");
	AddMenuItem(menu, info2, "BLU");
	SetMenuExitButton(menu, true);
	DisplayMenu(menu, client_id, 20);
}

public teamMenuHandler(Handle:menu, MenuAction:action, client_id, param)
{
	if (action == MenuAction_Select)
	{
		new String:info[32];
		GetMenuItem(menu, param, info, sizeof(info));
		new String:strsplit[2][32];
		ExplodeString(info, "|", strsplit, 2, 32);
		//ex info = "1|0" = "arena index | team selected"
		menuQueue(client_id, StringToInt(strsplit[0]), StringToInt(strsplit[1]));
	}
	else if (action == MenuAction_End)
	{
		CloseHandle(menu);
	}
}

//Setup and Display Queue Position Menu
public menuQueue(client_id, arena_index, team)
{
	new Handle:menu = CreateMenu(queueMenuHandler);
	
	decl String:title[192];
	Format(title, sizeof(title), "Select a queue position");
	
	SetMenuTitle(menu, title);

	new queue_length;
	queue_length = GetArraySize(queue[team][arena_index]);

	for (new queue_position_index = 0; queue_position_index < queue_length; queue_position_index++)
	{
		decl String:info[192];
		Format(info, sizeof(info), "%d|%d|%d", arena_index, team, queue_position_index);
		
		decl String:info_buffer[192] = "";

        //Player ids array of qwidth for given team and arena index
		new queue_position_ids[qwidth[team][arena_index]];
		GetArrayArray(queue[team][arena_index], queue_position_index, queue_position_ids);

		for (new queue_position_ids_index = 0; queue_position_ids_index < qwidth[team][arena_index]; queue_position_ids_index++)
		{
			decl String:playerid[192];
            //TODO convert player id to name (currently not done because of simulated arena content)
			IntToString(queue_position_ids[queue_position_ids_index], playerid, sizeof(playerid));
			StrCat(info_buffer, sizeof(info_buffer), playerid);
			StrCat(info_buffer, sizeof(info_buffer), ",");
		}
		
		AddMenuItem(menu, info, info_buffer);
	}
	SetMenuExitButton(menu, true);
	DisplayMenu(menu, client_id, 20);
}

public queueMenuHandler(Handle:menu, MenuAction:action, client_id, param)
{
	if (action == MenuAction_Select)
	{
		new String:info[32];
		GetMenuItem(menu, param, info, sizeof(info));
		new String:strsplit[3][32];
		ExplodeString(info, "|", strsplit, 3, 32);
		addToQueue(client_id, StringToInt(strsplit[0]), StringToInt(strsplit[1]), StringToInt(strsplit[2]));
	}
	else if (action == MenuAction_End)
	{
		CloseHandle(menu);
	}
}

//TODO check they're not adding to the same spot to avoid weird spawning stuff
public addToQueue(client_id, arena_index, team, queue_position_index)
{
    //Ensure client isn't added to more than arena at a time
	removeFromQueue(client_id);

	if (queue_position_index == -1) 
	{
        //-1 designates a qwidth of 1
		new client[1]
		client[0] = client_id;
		SetArrayArray(queue[0][arena_index], (GetArraySize(queue[0][arena_index]) - 1), client); 
	}
	else
	{
        //Get an array of player ids forthe selected queue position for the given team and arena index
		new queue_position_ids[qwidth[team][arena_index]];
		GetArrayArray(queue[team][arena_index], queue_position_index, queue_position_ids);

        //Ensure the queue position is not full of players
		new bool:slot_available = false;
		for (new queue_position_ids_index = 0; queue_position_ids_index < qwidth[team][arena_index]; queue_position_ids_index++)
		{
			if (queue_position_ids[queue_position_ids_index] == EMPTY_SLOT)
			{
				slot_available = true;
				queue_position_ids[queue_position_ids_index] = client_id;
				SetArrayArray(queue[team][arena_index], queue_position_index, queue_position_ids);
				break;
			}
		}
		if (slot_available == false)
			PrintToChat(client_id,"That queue has been filled, please add again");
	}

    //If a player switched queue position / team / arena they may have left a gap in the queue so clean it
	queueCleanup();

    //Check if the arena should become active
    if (arena_state[arena_index] == INACTIVE)
    {
        new ready_queue_position_indices[2];
        new ready_state = checkInactiveArenaReady(arena_index, ready_queue_position_indices);
        if (ready_state == READY)
        {
            //TODO
            //dequeue(arena_index, RED, 
            arena_state[arena_index] = ACTIVE;
        }
    }
    assignTeams(arena_index);
}

//TODO remove from active arena
public removeFromQueue(client_id)
{
    //TODO use findPlayer code
    //Look for client id in all arenas and replace with an empty slot
	for (new arena_index = 0; arena_index < arena_count; arena_index++)
	{
		for (new team = 0; team < 2; team++)
		{
            new queue_length;
            queue_length = GetArraySize(queue[team][arena_index]);

			for (new queue_position_index = 0; queue_position_index < queue_length; queue_position_index++)
			{
				debugMessage("Searching through arena %d, team %d, qpos %d", arena_index, team, queue_position_index);

				new queue_position_ids[qwidth[team][arena_index]];
				GetArrayArray(queue[team][arena_index], queue_position_index, queue_position_ids);

				for (new queue_position_ids_index = 0; queue_position_ids_index < qwidth[team][arena_index]; queue_position_ids_index++)
				{
					debugMessage("client id is %d compared against %d", queue_position_ids[queue_position_ids_index], client_id);
					if (queue_position_ids[queue_position_ids_index] == client_id)
					{
						queue_position_ids[queue_position_ids_index] = EMPTY_SLOT;
						SetArrayArray(queue[team][arena_index], queue_position_index, queue_position_ids);
						return;
					}
				}
			}
		}
	}
    //TODO assign spectator
}

public queueCleanup()
{
	for (new arena_index = 0; arena_index < arena_count; arena_index++)
	{
		for (new team = 0; team < 2; team++)
		{
			new array_index = 0;
			new queue_position_ids[qwidth[team][arena_index]];
			while (array_index < GetArraySize(queue[team][arena_index]))
			{
                //Go through each queue position and check if the entire queue position is empty
				GetArrayArray(queue[team][arena_index], array_index, queue_position_ids);
				new bool:empty_queue_pos = true;
				for (new queue_position_ids_index = 0; queue_position_ids_index < qwidth[team][arena_index]; queue_position_ids_index++)
				{
					if (queue_position_ids[queue_position_ids_index] != EMPTY_SLOT)
					{
						empty_queue_pos = false;
						break;
					}
				}
				
				if (!empty_queue_pos)
				{
                    //If the last queue position is not empty, add a queue position with empty slots
					if (array_index == GetArraySize(queue[team][arena_index]) - 1)
					{
						new empty_array[qwidth[team][arena_index]];
						for (new i = 0; i < qwidth[team][arena_index]; i++)
						{
							empty_array[i] = EMPTY_SLOT;
						}
						PushArrayArray(queue[team][arena_index], empty_array);
					}
					else
						array_index++;
				}
				else if (empty_queue_pos && array_index != (GetArraySize(queue[team][arena_index]) - 1))
				{
                    //Remove a queue position with all empty slots that's between 2 queue positions with players
					RemoveFromArray(queue[team][arena_index], array_index);
				}
				else
					array_index++;
			}
		}
	}
}

public shuffle(arena_index, team)
{
    new queue_length = GetArraySize(queue[team][arena_index]);
    for (new queue_position_index = 0; queue_position_index < queue_length - 1; queue_position_index++)
    {
        new queue_position_ids[qwidth[team][arena_index]];
        GetArrayArray(queue[team][arena_index], queue_position_index, queue_position_ids);
        
        //Check to see (starting at the first queue position) to see if there are empty slots that need filling
        new empty_slot_count = 0;
        for (new queue_position_ids_index = 0; queue_position_ids_index < qwidth[team][arena_index]; queue_position_ids_index++)
        {
            if (queue_position_ids[queue_position_ids_index] == EMPTY_SLOT)
                empty_slot_count++;
        }
        
        //TODO 2 iterations - exactly enough players followed by less than
        //If there are slots to be filled, look up the queue to see if theres a queue position with exactly enough players
        //to fill those empty slots, thus, leaving us with a full queue position that we can dequeue
        if (empty_slot_count > 0)
        {
            for (new later_queue_position_index = queue_position_index + 1; later_queue_position_index < queue_length; later_queue_position_index++)
            {
                new later_queue_position_ids[qwidth[team][arena_index]];
                GetArrayArray(queue[team][arena_index], later_queue_position_index, later_queue_position_ids);
            
                //Check how many players are in the later queue position
                new filled_slot_count = 0;
                for (new later_queue_position_ids_index = 0; later_queue_position_ids_index < qwidth[team][arena_index]; later_queue_position_ids_index++)
                {
                    if (later_queue_position_ids[later_queue_position_ids_index] != EMPTY_SLOT)
                        filled_slot_count++;
                }
                
                if (empty_slot_count == filled_slot_count)
                {
                    //Combine the later queue position (merging_players) into the current queue position (combined_players)
                    //to form queue position full of players 
                    new combined_players[qwidth[team][arena_index]];
                    GetArrayArray(queue[team][arena_index], queue_position_index, combined_players);
                    new merging_players[qwidth[team][arena_index]];
                    GetArrayArray(queue[team][arena_index], later_queue_position_index, merging_players);

                    new merging_players_index = 0;
                    for (new combined_players_index = 0; combined_players_index < qwidth[team][arena_index]; combined_players_index++)
                    {
                        if (combined_players[combined_players_index] == EMPTY_SLOT)
                        {
                            while (merging_players[merging_players_index] == EMPTY_SLOT)
                            {
                                merging_players_index++;
                            }
                            combined_players[combined_players_index] = merging_players[merging_players_index];
                            merging_players[merging_players_index] = EMPTY_SLOT;
                            merging_players_index++;
                        }
                    }
                    SetArrayArray(queue[team][arena_index], queue_position_index, combined_players);
                    SetArrayArray(queue[team][arena_index], later_queue_position_index, merging_players);
                    break;
                }
            }
        }
    }
	
    //If we did merge players into a queue position will have all empty slots so we'll do a clean up
	queueCleanup();
}

public getFullWaitingQueuePositions(arena_index, team, full_waiting_queue_position_indices[], num_positions_required)
{
    //TODO Use RED team for single queue arenas
    new queue_length; 
    queue_length = GetArraySize(queue[team][arena_index])
    
    //Check each queue position to see if it is full of players
    new ready_slot_index = 0
    for (new queue_position_index = 0; queue_position_index < queue_length; queue_position_index++)
    {
        new queue_position_ids[qwidth[team][arena_index]];
        GetArrayArray(queue[team][arena_index], queue_position_index, queue_position_ids);
        
        new bool:has_empty_slot = false;
        for (new queue_position_ids_index = 0; queue_position_ids_index < qwidth[team][arena_index]; queue_position_ids_index++)
        {
            if (queue_position_ids[queue_position_ids_index] == EMPTY_SLOT)
            {
                has_empty_slot = true;
                break;
            }
        }
        
        //If queue position is full, add it to the list of ready queue positions
        if (!has_empty_slot)
        {
            full_waiting_queue_position_indices[ready_slot_index] = queue_position_index;
            ready_slot_index++;
        } 
        
        //We only need a minimum of num_positions_required ready queue positions 
        //2 for single queue, 1 for separate queues
        if (ready_slot_index == num_positions_required)
        {
            return ready_slot_index;
        }
    }
    return ready_slot_index;
}

public checkInactiveArenaReady(arena_index, ready_queue_position_indices[])
{
    //Need 2 queue positions ready to become an active arena 

    if (arenaHasSharedSpawns(arena_index))
    {
        //Checking a single queue arena

        //Try to get 2 waiting positions and store the ready queue position indices in ready_queue_position_indices
        //Use RED team for single queue arenas
        new num_waiting_positions = getFullWaitingQueuePositions(arena_index, RED, ready_queue_position_indices, 2)

        if (num_waiting_positions == 2)
        {
            return READY;
        }
        else
        {
            return NOT_READY;
        }
    }
    else
    {
        //Checking a multi-queue arena
        
        //We'll store each teams next ready_queue_position_index in here (if they have one)
        new red_ready_queue_position_index[1];
        new blu_ready_queue_position_index[1];
        new num_red_waiting_positions = 0;
        new num_blu_waiting_positions = 0;

        //Checking multi queue arena
        num_red_waiting_positions = getFullWaitingQueuePositions(arena_index, RED, red_ready_queue_position_index, 1);
        num_blu_waiting_positions = getFullWaitingQueuePositions(arena_index, BLU, blu_ready_queue_position_index, 1);

        if (num_red_waiting_positions == 1 && num_blu_waiting_positions == 1)
        {
            ready_queue_position_indices[RED] = red_ready_queue_position_index[0];
            ready_queue_position_indices[BLU] = blu_ready_queue_position_index[0];
            return READY
        }
            
        return NOT_READY
    }
}

//TODO change return type to bool
public checkActiveTeamFull(arena_index, team)
{
    //returns the number of players in an active team
    new queue_position_ids[qwidth[team][arena_index]];
    GetArrayArray(active_players[team][arena_index], 0, queue_position_ids);

    new has_empty_slot = FALSE;
    for (new queue_position_ids_index = 0; queue_position_ids_index < qwidth[team][arena_index]; queue_position_ids_index++)
    {
        if (queue_position_ids[queue_position_ids_index] == EMPTY_SLOT)
        {
            has_empty_slot = TRUE;
            break;
        }
    }

    return has_empty_slot; 
}

public checkActiveArenaReady(arena_index, winning_team, losing_team, ready_queue_position_indices[])
{
    //TODO have to account for subs here, active_players teams need to be replaced

    ready_queue_position_indices[winning_team] = -1;
    ready_queue_position_indices[losing_team] = -1;

    new to_return = 0;

    //Check to see if losing team has a waiting team to replace them
    new losing_team_waiting_queue_position_index[1];
    new num_losing_team_waiting_positions = 0;
    num_losing_team_waiting_positions = getFullWaitingQueuePositions(arena_index, losing_team, losing_team_waiting_queue_position_index, 1);

    if (num_losing_team_waiting_positions > 0)
    {
        ready_queue_position_indices[losing_team] = losing_team_waiting_queue_position_index[0];
        to_return = to_return | REPLACE_LOSER;
    }
    else
    {
        //If nobody is waiting, see if the losing team is full
        if (!checkActiveTeamFull(arena_index, losing_team))
        {
            return NOT_READY;
        }
    }

    //Check to see if the winning team has enough players to remain
    if (!checkActiveTeamFull(arena_index, winning_team))
    {
        //If they don't, check to see if there is a full waiting team
        new winning_team_waiting_queue_position_index[1];
        new num_winning_team_waiting_positions = 0;
        num_winning_team_waiting_positions = getFullWaitingQueuePositions(arena_index, winning_team, winning_team_waiting_queue_position_index, 1);
        if (num_winning_team_waiting_positions > 0)
        {
            ready_queue_position_indices[winning_team] = winning_team_waiting_queue_position_index[0];
            to_return = to_return | REPLACE_WINNER;
            return to_return | READY;
        }
        else
        {   
            return NOT_READY
        }
    }
    else
    {
        return READY | to_return;
    }
}

public dequeue(arena_index, team, queue_position_index)
{
    new client_ids[qwidth[team][arena_index]];
    GetArrayArray(queue[team][arena_index], queue_position_index, client_ids);
    RemoveFromArray(queue[team][arena_index], queue_position_index);
    SetArrayArray(active_players[team][arena_index], 0, client_ids);
}

public requeue(arena_index, team)
{
    new client_ids[qwidth[team][arena_index]];
    GetArrayArray(active_players[team][arena_index], 0, client_ids);
    RemoveFromArray(active_players[team][arena_index], 0);
    PushArrayArray(queue[team][arena_index], client_ids);
    queueCleanup();
}

public requeueAtFront(arena_index, team)
{
    new client_ids[qwidth[team][arena_index]];
    GetArrayArray(active_players[team][arena_index], 0, client_ids);
    RemoveFromArray(active_players[team][arena_index], 0);
    //TODO InsertArrayArray? at position 0
    //PushArrayArray(players[team][arena_index], client_ids);
    queueCleanup();
}

//TODO handle all 'normal' respawns, we'll do the respawning ourselves

findPlayer(client_id, player_details[])
{
    //We'll store arena_index, team and queue_position_index in player_details
    for (new arena_index = 0; arena_index < arena_count; arena_index++)
    {
        for (new team = 0; team < 2; team++)
        {
            new queue_length; 
            queue_length = GetArraySize(queue[team][arena_index])
            
            //Search queues
            for (new queue_position_index = 0; queue_position_index < queue_length; queue_position_index++)
            {
                new queue_position_ids[qwidth[team][arena_index]];
                GetArrayArray(queue[team][arena_index], queue_position_index, queue_position_ids);
                
                for (new queue_position_ids_index = 0; queue_position_ids_index < qwidth[team][arena_index]; queue_position_ids_index++)
                {
                    if (queue_position_ids[queue_position_ids_index] == client_id)
                    {
                        
                        player_details[arena_ix] = arena_index;
                        player_details[team_ix] = team;
                        player_details[queue_position_ix] = queue_position_index;
                        return PLAYER_QUEUED;
                    }
                }
            }

            //Search active slots 
            queue_length = GetArraySize(active_players[team][arena_index])
            
            for (new queue_position_index = 0; queue_position_index < queue_length; queue_position_index++)
            {
                new queue_position_ids[qwidth[team][arena_index]];
                GetArrayArray(active_players[team][arena_index], queue_position_index, queue_position_ids);
                
                for (new queue_position_ids_index = 0; queue_position_ids_index < qwidth[team][arena_index]; queue_position_ids_index++)
                {
                    if (queue_position_ids[queue_position_ids_index] == client_id)
                    {
                        
                        player_details[arena_ix] = arena_index;
                        player_details[team_ix] = team;
                        player_details[queue_position_ix] = queue_position_index;
                        return PLAYER_ACTIVE;
                    }
                }
            }

        }
    }
    return PLAYER_IDLE;
}

//TODO hook this to the actual playerdeath function
public playerDeath(client_id)
{
    //Respawn a player if necessary
    new player_details[PLAYERDETAILS];
    new player_queue_state = findPlayer(client_id, player_details);

    //Player doesn't need to spawn if they're not in an arena
    if (player_queue_state != PLAYER_IDLE)    
    {
        //get their arena info
        new arena_index = player_details[arena_ix]
        new team = player_details[team_ix]
        new queue_position_index = player_details[queue_position_ix];
        player_state[client_id] = DEAD

        if (arena_state[arena_index] == ACTIVE)
        {
            //this statement is not necessarily true, but it's an assumption to save some code
            new winning_team = otherTeam(team)
            new losing_team = team

            if (options[arena_index][game_type] == ELIMINATION)
            {
                if (checkTeamEliminated(arena_index, team) == TRUE)                    
                {
                    //give points to the team who just eliminated the other team
                    points[winning_team][arena_index]++;
                    if (points[winning_team][arena_index] == options[arena_index][point_limit])
                    {
                        prepareNextMatch(arena_index, winning_team, losing_team)
                    }
                    else
                    {
                        prepareNextRound(arena_index, winning_team, losing_team)
                    }
                } 
            }
            else if (options[arena_index][game_type] == DEATHMATCH)
            {
                points[winning_team][arena_index]++;
                if (points[winning_team][arena_index] == options[arena_index][point_limit])
                {
                    prepareNextMatch(arena_index, winning_team, losing_team)
                }
                else
                {
                    //TODO respawnPlayer() on timer
                }
            }
            else if (options[arena_index][game_type] == BBALL)
            {
                //TODO not implemented yet
            }
        }
        else
        {
            respawnPlayer()
        }
    }
}

public prepareNextMatch(arena_index, winning_team, losing_team)
{
    //store the replacement queue positions in here if necessary
    new ready_queue_position_indices[2]

    new ready_state = checkActiveArenaReady(arena_index, winning_team, losing_team, ready_queue_position_indices)
    if (ready_state & READY)
    {
        //Don't have to replace teams if there's no waiting team queued up
        //Attempt to replace teams (even winners) if they don't have enough players or if they had a sub
        if (ready_state & REPLACE_WINNER)
        {
            requeue(arena_index, winning_team);
//TODO check Check*ArenaReady(...) compensates for shared queues
//TODO if hasSharedSpawns dequeue(RED, ready_queue_position_indices)
            dequeue(arena_index, winning_team, ready_queue_position_indices[winning_team]);
        }

        if (ready_state & REPLACE_LOSER)
        {
            requeue(arena_index, losing_team);
//TODO if hasSharedSpawnsDequeue(RED)
            dequeue(arena_index, losing_team, ready_queue_position_indices[losing_team]);
        }

        arena_state[arena_index] = ACTIVE;
        points[winning_team][arena_index] = 0;
        points[losing_team][arena_index] = 0;
        assignTeams(arena_index)
        //TODO respawnTeams()
    }
    else
    {
        //arena doesn't have any full teams so requeue winners at the front of their queue
        //and requeue the losers at the back of their queue
        requeueAtFront(arena_index, winning_team);
        requeue(arena_index, losing_team);
        
        arena_state[arena_index] = INACTIVE;
        assignTeams(arena_index)
        //respawnTeams()
        //TODO setArenaPreGame
            //set arena_state = INACTIVE
            //spawn players
    }
}

public assignTeams(arena_index)
{
    if (arena_state[arena_index] == ACTIVE)
    {
        for (new team = 0; team < 2; team++)
        {
            //have the 2 ready teams join teams
            new queue_length; 
            queue_length = GetArraySize(active_players[team][arena_index])
            
            for (new queue_position_index = 0; queue_position_index < queue_length; queue_position_index++)
            {
                new queue_position_ids[qwidth[team][arena_index]];
                GetArrayArray(active_players[team][arena_index], queue_position_index, queue_position_ids);
                
                for (new queue_position_ids_index = 0; queue_position_ids_index < qwidth[team][arena_index]; queue_position_ids_index++)
                {
                    //shouldn't ever be zero (I think), cause we'll calling assignTeams only when 2 ready teams were dequeued
                    if (queue_position_ids[queue_position_ids_index] != 0)
                    {
                        //TODO if team != getTeam
                        //TODO jointeam(queue_position_ids[queue_position_ids_index], team)    
                    }
                }
            }
            
            //have the queued teams join spectators
            queue_length = GetArraySize(queue[team][arena_index])

            for (new queue_position_index = 0; queue_position_index < queue_length; queue_position_index++)
            {
                new queue_position_ids[qwidth[team][arena_index]];
                GetArrayArray(queue[team][arena_index], queue_position_index, queue_position_ids);
                
                for (new queue_position_ids_index = 0; queue_position_ids_index < qwidth[team][arena_index]; queue_position_ids_index++)
                {
                    //shouldn't ever be zero I don't think, cause we'll calling assignTeams only when 2 ready teams were dequeued
                    if (queue_position_ids[queue_position_ids_index] != 0)
                    {
                        //TODO if team != getTeam
                        //TODO joinSpec(queue_position_ids[queue_position_ids_index])    
                    }
                }
            }
        }    
    }    
    else
    {
        //active players should already be requeued at this point but just incase put any players in an active slot, in spec 
        for (new team = 0; team < 2; team++)
        {
            new queue_length; 
            queue_length = GetArraySize(active_players[team][arena_index])
            
            for (new queue_position_index = 0; queue_position_index < queue_length; queue_position_index++)
            {
                new queue_position_ids[qwidth[team][arena_index]];
                GetArrayArray(active_players[team][arena_index], queue_position_index, queue_position_ids);
                
                for (new queue_position_ids_index = 0; queue_position_ids_index < qwidth[team][arena_index]; queue_position_ids_index++)
                {
                    //shouldn't ever be zero I don't think, cause we'll calling assignTeams only when 2 ready teams were dequeued
                    if (queue_position_ids[queue_position_ids_index] != 0)
                    {
                        //TODO if team != getTeam
                        //TODO joinSpec(queue_position_ids[queue_position_ids_index])    
                    }
                }
            }
        }    

        if (arenaHasSharedSpawns(arena_index))
        {
            //working with a single queue, use RED team
            new queue_length; 
            queue_length = GetArraySize(queue[RED][arena_index])
            
            for (new queue_position_index = 0; queue_position_index < queue_length; queue_position_index++)
            {
                new queue_position_ids[qwidth[RED][arena_index]];
                GetArrayArray(queue[RED][arena_index], queue_position_index, queue_position_ids);
                
                for (new queue_position_ids_index = 0; queue_position_ids_index < qwidth[RED][arena_index]; queue_position_ids_index++)
                {
                    //shouldn't ever be zero I don't think, cause we'll calling assignTeams only when 2 ready teams were dequeued
                    if (queue_position_ids[queue_position_ids_index] != 0)
                    {
                        //first 2 queue positions can play pregame
                        if (queue_position_index < 2)
                        {
                            //TODO if team != getTeam
                            if (queue_position_index == RED)
                            {
                                //TODO joinTeam(queue_position_ids[queue_position_ids_index], RED)    
                            }
                            else
                            {
                                //TODO joinTeam(queue_position_ids[queue_position_ids_index], BLU)    
                            }
                        }
                        else
                        {
                            //TODO if team != getTeam
                            //TODO joinSpec(queue_position_ids[queue_position_ids_index])    
                        }
                    }
                }
            }
        }
    }
}

/*public assignPlayerToTeam(client_id, arena_index, team)
{
    if (arena_state[arena_index] == ACTIVE)
    {
        
    }
    //is active
        //is in active_players
            //assign active_players team
        //else
            //assign spec
    //is not active
        //is single queue
            //is qpos 0 
                assigne red
            //is qpos 1
                assign blue
        //else
            assign team
                
}*/

public prepareNextRound(arena_index, winning_team, losing_team)
{
    //TODO respawnPlayers()
    //set shooting delay
}

public respawnTeams(arena_index)
{
    if (arena_state[arena_index] == ACTIVE)
    {
        for (new team = 0; team < 2; team++)
        {
            new queue_length; 
            queue_length = GetArraySize(active_players[team][arena_index])
            
            for (new queue_position_index = 0; queue_position_index < queue_length; queue_position_index++)
            {
                new queue_position_ids[qwidth[team][arena_index]];
                GetArrayArray(active_players[team][arena_index], queue_position_index, queue_position_ids);
                
                for (new queue_position_ids_index = 0; queue_position_ids_index < qwidth[team][arena_index]; queue_position_ids_index++)
                {
                    //Incase someone has left, their spawn will be empty
                    if (queue_position_ids[queue_position_ids_index] != 0)
                    {
                        //TODO if separate spawn points
                        /*new num_spawns = GetArraySize(spawn_points[team][arena_index]);
                        spawn_xy = GetArrayArray(spawn_point[team][arena_index], spawn_index[team][arena_index] % num_spawns);
                        spawn_angles = GetArrayArray(spawn_orientation[team][arena_index], spawn_index[team][arena_index] % num_spawns);
                        spawn_index[team][arena_index]++;*/
                        
                        //TODO spawn(queue_position_ids[queue_position_ids_index], spawn_xy[x], spawn_xy[y], spawn_orientation[....)
                    }
                }
            }
        }
    }
    else
    {
        //if single /multiqueue
        if (arenaHasSharedSpawns(arena_index))
        {
            //grab players from the first 2 queue positions
            for (new queue_position_index = 0; queue_position_index < 2; queue_position_index++)
            {
                new queue_position_ids[qwidth[RED][arena_index]];
                GetArrayArray(active_players[RED][arena_index], queue_position_index, queue_position_ids);
                
                for (new queue_position_ids_index = 0; queue_position_ids_index < qwidth[RED][arena_index]; queue_position_ids_index++)
                {
                    if (queue_position_ids[queue_position_ids_index] != 0)
                    {
                        //if separate spawns
                        //TODO if separate spawn points
                        new team = RED
                        if (queue_position_index == 1)
                            team = BLU

                        /*new num_spawns;
                        num_spawns = GetArraySize(spawn_points[team][arena_index]);
                        spawn_xy = GetArrayArray(spawn_point[team][arena_index], spawn_index[team][arena_index] % num_spawns);
                        spawn_angles = GetArrayArray(spawn_orientation[team][arena_index], spawn_index[team][arena_index] % num_spawns);
                        spawn_index[team][arena_index]++;*/
                    }
                }
            }
        }
        
    }
    /*set player state from DEAD to alive
    //Is active?
        spawn from active_players
    //Is inactive?
        check single queue
            spawn from queue position 0*/
}

public respawnPlayer()
{
   //TODO
   //set player state from DEAD to alive
}

public otherTeam(team)
{
    if (team == RED)
        return BLU;
    else
        return RED;
}

//TODO onConnect
//TODO onDisconnect

public checkTeamEliminated(arena_index, team)
{
    //Check to see if every players state for a given team is not equal to ALIVE
    new queue_length; 
    queue_length = GetArraySize(active_players[team][arena_index])
    
    for (new queue_position_index = 0; queue_position_index < queue_length; queue_position_index++)
    {
        new queue_position_ids[qwidth[team][arena_index]];
        GetArrayArray(active_players[team][arena_index], queue_position_index, queue_position_ids);
        
        new entire_team_eliminated = TRUE;
        for (new queue_position_ids_index = 0; queue_position_ids_index < qwidth[team][arena_index]; queue_position_ids_index++)
        {
            if (player_state[queue_position_ids[queue_position_ids_index]] == ALIVE)
            {
                entire_team_eliminated = FALSE;
            }
        }
        return entire_team_eliminated; 
    }
}

public queueDebug(client_id)
{
	PrintToConsole(client_id, "/////////////////\nMQA DEBUG\n/////////////////");
	for (new arena_index = 0; arena_index < MAX_ARENAS; arena_index++)
	{
		PrintToConsole(client_id, "Arena: %s",arena[arena_index]);
        if (arenaHasSharedSpawns(arena_index))
        {
            PrintToConsole(client_id, "Shared Queue:\n");

            new queue_length; 
            queue_length = GetArraySize(active_players[RED][arena_index])
            
            for (new queue_position_index = 0; queue_position_index < queue_length; queue_position_index++)
            {
                new queue_position_ids[qwidth[RED][arena_index]];
                GetArrayArray(active_players[RED][arena_index], queue_position_index, queue_position_ids);
                
                new entire_team_eliminated = TRUE;
                for (new queue_position_ids_index = 0; queue_position_ids_index < qwidth[RED][arena_index]; queue_position_ids_index++)
                {
                    PrintToConsole(client_id, "Client ID: %i", queue_position_ids[queue_position_ids_index]);
                }
            }
            PrintToConsole(client_id, "\n");
        }
        else
        {
            for (new team = 0; team < 2; team++)
            {
                if (team == RED)
                {
                    PrintToConsole(client_id, "Red Queue:\n");
                }
                else
                {
                    PrintToConsole(client_id, "Blue Queue:\n");
                }

                new queue_length; 
                queue_length = GetArraySize(active_players[team][arena_index])
                
                for (new queue_position_index = 0; queue_position_index < queue_length; queue_position_index++)
                {
                    new queue_position_ids[qwidth[team][arena_index]];
                    GetArrayArray(active_players[team][arena_index], queue_position_index, queue_position_ids);
                    
                    new entire_team_eliminated = TRUE;
                    for (new queue_position_ids_index = 0; queue_position_ids_index < qwidth[team][arena_index]; queue_position_ids_index++)
                    {
                        PrintToConsole(client_id, "Client ID: %i", queue_position_ids[queue_position_ids_index]);
                    }
                }
                PrintToConsole(client_id, "\n");
            }
        }
	}
}



public OnPluginEnd()
{
	/*
		Probably going to kill timers and whatever could potentially cause a leak here.
	*/
}
public OnMapStart()
{
	/*
		OnMapStart() is where we will start global timers, get the map spawns, cache sounds, and stuff like that.
	*/
}
public OnMapEnd()
{
	/*
		Probably going to kill timers and whatever could potentially cause a leak here.
	*/
}
public OnConfigsExecuted()
{
	/*
		This is called after all of the servers configs are executed, guarantees that we're using the correct variables.
	*/
	options_enabled = GetConVarBool(cvar_enabled);
	options_debug = GetConVarBool(cvar_debug);
}
public OnClientPostAdminCheck(client)
{
	/*
		After testing I found this to be the very last call for when a client joins a server, making it the ideal time to declare client shiz.
	*/
}

public debugMessage(String:message[], any:...)
{
	if(options_debug)
	{
		decl String:buffer[192];
		VFormat(buffer, sizeof(buffer), message, 2);
		new String:msg[] = "\x04[MQA Debug] \x01";
		StrCat(msg, sizeof(buffer), buffer);
		PrintToChatAll(msg);
	}
}

public Action:cmdVersion(client_id, args)
{
	if (!client_id) return Plugin_Handled; //if client doesn't exist
		
	PrintToChat(client_id, "%s v%s", PLUGIN_NAME, PLUGIN_VERSION);
	return Plugin_Handled;
}

public cvarCMDChanged(Handle:cvar, const String:oldValue[], const String:newValue[])
{
	if (cvar == cvar_enabled)
		StringToInt(newValue) ? (options_enabled = true) : (options_enabled = false);
	else if (cvar == cvar_debug)
		StringToInt(newValue) ? (options_debug = true) : (options_debug = false);
}
